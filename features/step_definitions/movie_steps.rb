# Add a declarative step here for populating the DB with movies.

Given /the following movies exist/ do |movies_table|

  @my_movies = {'G' => [], 'R' => [], 'PG-13' => [], 'PG' => []}
  count = {'G' => 0, 'R' => 0, 'PG-13' => 0, 'PG' => 0,}

  @movies_count = movies_table.hashes.length
  movies_table.hashes.each do |movie|
    # each returned element will be a hash whose key is the table header.
    # you should arrange to add that movie to the database here
    Movie.create!(movie)
    r = movie[:rating]
    @my_movies[r][count[r]] = movie[:title]
    count[r] += 1
  end
end

# Make sure that one string (regexp) occurs before or after another one
# on the same page

Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.content  is the entire content of the page as a string.
  regexp = /#{e1}.*#{e2}/m    #  /m means match across newlines   
  page.body.should =~ regexp
end

Then /I should(n't)? see movies with ratings: (.*)/ do |absence, rating_str|
  #  page.content  is the entire content of the page as a string.
  result = true
  count = 0
  ratings = rating_str.split(/,\s*/)
  presence = (absence == nil)
  ratings.each do |r|
    @my_movies[r].each do |title|
      if (presence && page.body.include?(title) == true) ||
         (absence && page.body.include?(title) == false)
        count = count + 1
      end
    end
    result = result && (@my_movies[r].count == count)
    count = 0
  end
  result.should == true
end

Then /^I should see all of the movies$/ do
  table_data_rows = page.body.scan("<tr>").count - 1
  table_data_rows.should == @movies_count
end

# Make it easier to express checking or unchecking several boxes at once
#  "When I uncheck the following ratings: PG, G, R"
#  "When I check the following ratings: G"

When /I (un)?check the following ratings: (.*)/ do |uncheck, rating_list|
  # HINT: use String#split to split up the rating_list, then
  #   iterate over the ratings
  rating_list.split(/,\s*/).each do |code|
    uncheck == "un"? uncheck("ratings_#{code}"): check("ratings_#{code}")
  end

  # and reuse the "When I check..." or
  #   "When I uncheck..." steps in lines 89-95 of web_steps.rb
end
